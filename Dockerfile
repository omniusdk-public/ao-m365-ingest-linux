ARG fromTag=7.4-alpine-3.17
ARG installerRepo=mcr.microsoft.com/powershell

FROM ${installerRepo}:${fromTag}

COPY ./ingest_service .

ENV CHOCO_URL=https://chocolatey.org/install.ps1
ENV chocolateyVersion=1.4.0

RUN pwsh install_deps.ps1
RUN pwsh install_modules.ps1

CMD ["pwsh", "./ingest.ps1"]
