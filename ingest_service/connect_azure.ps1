function ConnectAzMSI {
    try {
        Write-Log -Level "INFO" -Message "Attempting to connect to Az using MSI"
        Import-Module Az.Resources
        Import-Module Az.Accounts
        Import-Module Az.KeyVault
        Connect-AzAccount -Identity
        $az_connection = $true
    }
    catch {
        Write-Host $_
        Write-Log -Level "ERROR" -Message "Failed to Connect to Az using MSI"
        $az_connection = $false
    }

    return $az_connection
}

function ConnectAzBasicOrCert($UseServicePrincipal, $Cert, $M365TenantId, $ServicePrincipalId, $Cred) {
    try {
        Write-Log -Level "INFO" -Message "Connecting to Az module"
        Import-Module Az.Resources
        Import-Module Az.Accounts
        Import-Module Az.KeyVault
        if ($UseServicePrincipal -eq "true") {
            Connect-AzAccount -CertificateThumbprint $Cert.Thumbprint -TenantId $M365TenantId -ApplicationId $ServicePrincipalId
        }
        else {
            Connect-AzAccount -Credential $Cred -Tenant $M365TenantId
        }
    }
    catch {
        Write-Log -Level "ERROR" -Message "Error connecting to Az Powershell Module"
        Write-Host $_
        Write-Log -Level "INFO" -Message "Azure cmdlets will not be successfully invoked and azure data will not be sent to AppOmni"
    }
}
