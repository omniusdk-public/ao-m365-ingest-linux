function ConnectExchangeMSI($OrgUrl) {
    try {
        Write-Log -Level "INFO" -Message "Attempting to connect to Exchange using MSI"
        Connect-ExchangeOnline -ManagedIdentity -Organization $OrgUrl
        $exchange_connection = $true
    }
    catch {
        Write-Host $_
        Write-Log -Level "ERROR" -Message "Failed to Connect to Exchange using MSI"
        $exchange_connection = $false
    }

    return $exchange_connection

}

function ConnectExchangeBasicOrCert ($UseServicePrincipal, $ServicePrincipalId, $CertFilePath, $OrgUrl, $PFXPass, $Cred) {
    try {
        Write-Log -Level "INFO" -Message "Connecting to Exchange Powershell Module"
        if ($UseServicePrincipal -eq "true") {
            # seen issues with using -CertificateThumprint with connecting to Exchange - using -CertificateFilePath
            Connect-ExchangeOnline -AppId $ServicePrincipalId -CertificateFilePath $CertFilePath -Organization $OrgUrl -CertificatePassword $pfxpass
        }
        else {
            Connect-ExchangeOnline -Credential $Cred -Organization $OrgUrl
        }
    }
    catch {
        Write-Log -Level "ERROR" -Message "Error connecting to Exchange Online"
        Write-Host $_
        Write-Log -Level "INFO" -Message "Exchange cmdlets will not be successfully invoked and exchange data will not be sent to AppOmni"
    }
}
