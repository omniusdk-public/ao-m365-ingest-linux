# To be used in Docker deployment

if ($true -eq $IsWindows) {
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]'Tls,Tls11,Tls12'
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString("$env:CHOCO_URL"))
    choco install vault -y
} elseif ($true -eq $IsLinux) {
    apk update && apk add vault
}
