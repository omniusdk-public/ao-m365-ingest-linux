function CreatePnPManagementShellServicePrincipal ($SharePointUrl, $ServicePrincipalId, $Cert, $OrgUrl, $Cred){
    $PnPPowerShellAppId = "31359c7f-bd7e-475c-86db-fdb8c937548e"

    Write-Log -Level "INFO" -Message "Getting Access Token"
    $accessTokenInformation = Get-AzAccessToken –ResourceUrl "https://graph.microsoft.com"
    $token = $accessTokenInformation.Token


    Write-Log -Level "INFO" -Message "Attempting to get PnP Management Shell Service Principal"
    $pnpServicePrincipal = Get-AzADServicePrincipal –ApplicationId  $PnPPowerShellAppId

    if ($null -eq $pnpServicePrincipal) {
        Write-Log -LEVEL "INFO" -Message "No PnP Management Shell Service Principal; creating..."
        $pnpServicePrincipal = New-AzADServicePrincipal -ApplicationId $PnPPowerShellAppId
    }
    Write-Log -Level "INFO" -Message "Getting Sharepoint Service Principal"
    $spServicePrincipal = Get-AzADServicePrincipal –ApplicationId "00000003-0000-0ff1-ce00-000000000000"

    $requestedScopes = "User.ReadWrite.All Group.Read.All AllSites.FullControl"

    $payload = @{
            clientId = $pnpServicePrincipal.Id
            consentType = "AllPrincipals"
            principalId = $null
            resourceId = $spServicePrincipal.Id
            scope = $requestedScopes
    } | ConvertTo-Json


    Write-Log -Level "INFO" -Message "Granting User.ReadWrite.All Group.Read.All AllSites.FullControl Permissions to PnP Management Shell Service Principal"
    Invoke-RestMethod –Uri "https://graph.microsoft.com/v1.0/oauth2PermissionGrants" –Headers @{Authorization = "Bearer $token"} –Body $payload –Method "POST" –ContentType "application/json"

    # sleeping for 100 secs here to stop race condition where connection is attempted before service principal is fully created
    Write-Log -Level "INFO" -Message "Sleeping for 100 seconds to handle race condition where connection is attempted before service principal is created"
    Start-Sleep 100

    Write-Log -Level "INFO" -Message "Connecting to PnP Powershell again"
    try {
        if ($env:use_msi -eq "true") {
            Connect-PnPOnline -ManagedIdentity -Url $SharePointUrl
            return $true
        }
        elseif ($env:use_service_principal -eq "true") {
            Connect-PnPOnline -Url $SharePointUrl -ClientId $ServicePrincipalId -Thumbprint $Cert.Thumbprint -Tenant $OrgUrl
        }
        else {
            Connect-PnPOnline -Url $SharePointUrl -Credentials $Cred
        }
    }
    catch {
        Write-Log -Level "ERROR" -Message "Error connecting to PnP powershell again"
        Write-Host $_
        Write-Log -Level "INFO" -Message "PnP cmdlets will not be successfully invoked and sharepoint data will not be sent to AppOmni"
    }

}

function ConnectPnPMSI ($SharePointUrl, $UserManagedIdentityObjectId = $null){
    try {
        Write-Log -Level "INFO" -Message "Attempting to connect to PnP using MSI"
        if ($null -ne $UserManagedIdentityObjectId) {
            Connect-PnPOnline -ManagedIdentity -UserAssignedManagedIdentityObjectId $UserManagedIdentityObjectId -Url $SharePointUrl
        } else {
            Connect-PnPOnline -ManagedIdentity -Url $SharePointUrl
        }
        $pnp_connection = $true
    }
    catch {
        Write-Host $_
        if ($_.Exception.Message.Contains("user or administrator has not consented")) {
            Write-Log -Level "INFO" -Message "Need to create or update scopes for PnP Management Shell Service Principal. This will only happen once"
            return CreatePnPManagementShellServicePrincipal -SharePointUrl $SharePointUrl
        }
        else {
            Write-Log -Level "ERROR" -Message "Error conecting to PnP using MSI"
        }

    }
    return $pnp_connection
}

function ConnectPnPBasicOrCert ($SharePointUrl, $ServicePrincipalId, $Cert, $OrgUrl, $Cred) {
    try {
        Write-Log -Level "INFO" -Message "Connecting to PnP Powershell"
        if ($env:use_service_principal -eq "true") {
            Connect-PnPOnline -Url $SharePointUrl -ClientId $ServicePrincipalId -Thumbprint $Cert.Thumbprint -Tenant $OrgUrl
        }
        else {
            Connect-PnPOnline -Url $SharePointUrl -Credentials $Cred
        }
    }
    catch {
        # if we cannot connect to PnP powershell with the specific error below, it is because this is the first time connecting
        # in this case, the PnP Management Shell Service Principal must be created and given specific scopes
        # this is the equivalent of running the Register-PnPManagementShellAccess, however that command is interactive
        Write-Host $_
        if ($_.Exception.Message.Contains("user or administrator has not consented")) {
            Write-Log -Level "INFO" -Message "Need to create or update scopes for PnP Management Shell Service Principal. This will only happen once"
            CreatePnPManagementShellServicePrincipal -SharePointUrl $SharePointUrl -ServicePrincipalId $ServicePrincipalId -Cert $Cert -OrgUrl $OrgUr - Cred $Cred
        }
        else {
            Write-Log -Level "ERROR" -Message "Error connecting to PnP powershell - exiting"
            Write-Host $_
            Write-Log -Level "INFO" -Message "PnP cmdlets will not be successfully invoked and sharepoint data will not be sent to AppOmni"
        }

    }
}
